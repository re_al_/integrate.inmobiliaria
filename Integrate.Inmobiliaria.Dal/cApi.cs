namespace Integrate.Inmobiliaria.Dal
{
    public static class cApi
    {
        public enum Estado
        {
            ELABORADO,
            FINAL,
            INICIAL
        }

        public enum Transaccion
        {
            CREAR,
            ELIMINAR,
            MODIFICAR
        }
    }
}

