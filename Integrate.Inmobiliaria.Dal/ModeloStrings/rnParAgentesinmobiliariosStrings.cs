#region 
/***********************************************************************************************************
	NOMBRE:       rnParAgentesinmobiliarios
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla par_agentes_inmobiliarios

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        18/05/2016  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Integrate.Inmobiliaria.Dal; 
using Integrate.Inmobiliaria.SqLiteConn; 
using Integrate.Inmobiliaria.Dal.Entidades;
using Integrate.Inmobiliaria.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace Integrate.Inmobiliaria.Dal.Modelo
{
	public partial class rnParAgentesinmobiliarios: inParAgentesinmobiliarios
	{
		public const string strNombreLisForm = "Listado de par_agentes_inmobiliarios";
		public const string strNombreDocForm = "Detalle de par_agentes_inmobiliarios";
		public const string strNombreForm = "Registro de par_agentes_inmobiliarios";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entParAgentesinmobiliarios.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entParAgentesinmobiliarios.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entParAgentesinmobiliarios.Fields.agente_inmobiliario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo integer de la tabla par_agentes_inmobiliarios";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo integer de la tabla par_agentes_inmobiliarios";
					return "Columna de tipo integer de la tabla par_agentes_inmobiliarios";
				}
				if (entParAgentesinmobiliarios.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
					return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
				}
				if (entParAgentesinmobiliarios.Fields.correo_electronico == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
					return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
				}
				if (entParAgentesinmobiliarios.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
					return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entParAgentesinmobiliarios.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entParAgentesinmobiliarios.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entParAgentesinmobiliarios.Fields.agente_inmobiliario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para agente_inmobiliario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para agente_inmobiliario";
					return "int para agente_inmobiliario";
				}
				if (entParAgentesinmobiliarios.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entParAgentesinmobiliarios.Fields.correo_electronico == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para correo_electronico";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para correo_electronico";
					return "String para correo_electronico";
				}
				if (entParAgentesinmobiliarios.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para observaciones";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para observaciones";
					return "String para observaciones";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entParAgentesinmobiliarios.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entParAgentesinmobiliarios.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entParAgentesinmobiliarios.Fields.agente_inmobiliario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "agente_inmobiliario:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "agente_inmobiliario:";
					return "agente_inmobiliario:";
				}
				if (entParAgentesinmobiliarios.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entParAgentesinmobiliarios.Fields.correo_electronico == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "correo_electronico:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "correo_electronico:";
					return "correo_electronico:";
				}
				if (entParAgentesinmobiliarios.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "observaciones:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "observaciones:";
					return "observaciones:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

