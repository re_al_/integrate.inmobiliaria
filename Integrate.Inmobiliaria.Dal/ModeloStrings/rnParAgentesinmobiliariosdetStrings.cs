#region 
/***********************************************************************************************************
	NOMBRE:       rnParAgentesinmobiliariosdet
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla par_agentes_inmobiliarios_det

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/05/2016  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Integrate.Inmobiliaria.Dal; 
using Integrate.Inmobiliaria.SqLiteConn; 
using Integrate.Inmobiliaria.Dal.Entidades;
using Integrate.Inmobiliaria.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace Integrate.Inmobiliaria.Dal.Modelo
{
	public partial class rnParAgentesinmobiliariosdet: inParAgentesinmobiliariosdet
	{
		public const string strNombreLisForm = "Listado de par_agentes_inmobiliarios_det";
		public const string strNombreDocForm = "Detalle de par_agentes_inmobiliarios_det";
		public const string strNombreForm = "Registro de par_agentes_inmobiliarios_det";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entParAgentesinmobiliariosdet.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entParAgentesinmobiliariosdet.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entParAgentesinmobiliariosdet.Fields.agente_inmobiliario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det";
					return "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det";
				}
				if (entParAgentesinmobiliariosdet.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo numeric(10, 0) de la tabla par_agentes_inmobiliarios_det";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo numeric(10, 0) de la tabla par_agentes_inmobiliarios_det";
					return "Columna de tipo numeric(10, 0) de la tabla par_agentes_inmobiliarios_det";
				}
				if (entParAgentesinmobiliariosdet.Fields.tipo_telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios_det";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios_det";
					return "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios_det";
				}
				if (entParAgentesinmobiliariosdet.Fields.activo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det";
					return "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entParAgentesinmobiliariosdet.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entParAgentesinmobiliariosdet.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entParAgentesinmobiliariosdet.Fields.agente_inmobiliario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para agente_inmobiliario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para agente_inmobiliario";
					return "int para agente_inmobiliario";
				}
				if (entParAgentesinmobiliariosdet.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para telefono";
					return "int para telefono";
				}
				if (entParAgentesinmobiliariosdet.Fields.tipo_telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para tipo_telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para tipo_telefono";
					return "String para tipo_telefono";
				}
				if (entParAgentesinmobiliariosdet.Fields.activo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para activo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para activo";
					return "int para activo";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entParAgentesinmobiliariosdet.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entParAgentesinmobiliariosdet.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entParAgentesinmobiliariosdet.Fields.agente_inmobiliario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "agente_inmobiliario:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "agente_inmobiliario:";
					return "agente_inmobiliario:";
				}
				if (entParAgentesinmobiliariosdet.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "telefono:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "telefono:";
					return "telefono:";
				}
				if (entParAgentesinmobiliariosdet.Fields.tipo_telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "tipo_telefono:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "tipo_telefono:";
					return "tipo_telefono:";
				}
				if (entParAgentesinmobiliariosdet.Fields.activo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "activo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "activo:";
					return "activo:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

