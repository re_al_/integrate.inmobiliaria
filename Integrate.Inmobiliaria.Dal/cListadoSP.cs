namespace Integrate.Inmobiliaria.Dal
{
    public class cListadoSP
    {

        public enum SegAplicaciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegAplicaciones
            /// </summary>
            SpSapIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegAplicaciones
            /// </summary>
            SpSapUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegAplicaciones
            /// </summary>
            SpSapDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegAplicaciones
            /// </summary>
            spSapInsUpd
        }

        public enum SegEstados
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegEstados
            /// </summary>
            SpSesIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegEstados
            /// </summary>
            SpSesUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegEstados
            /// </summary>
            SpSesDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegEstados
            /// </summary>
            spSesInsUpd
        }

        public enum SegMensajeserror
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegMensajeserror
            /// </summary>
            SpSmeIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegMensajeserror
            /// </summary>
            SpSmeUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegMensajeserror
            /// </summary>
            SpSmeDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegMensajeserror
            /// </summary>
            spSmeInsUpd
        }

        public enum SegPaginas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegPaginas
            /// </summary>
            SpSpgIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegPaginas
            /// </summary>
            SpSpgUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegPaginas
            /// </summary>
            SpSpgDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegPaginas
            /// </summary>
            spSpgInsUpd
        }

        public enum SegParametros
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegParametros
            /// </summary>
            SpSpaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegParametros
            /// </summary>
            SpSpaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegParametros
            /// </summary>
            SpSpaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegParametros
            /// </summary>
            spSpaInsUpd
        }

        public enum SegParametrosdet
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegParametrosdet
            /// </summary>
            SpSpdIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegParametrosdet
            /// </summary>
            SpSpdUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegParametrosdet
            /// </summary>
            SpSpdDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegParametrosdet
            /// </summary>
            spSpdInsUpd
        }

        public enum SegRoles
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegRoles
            /// </summary>
            SpSroIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegRoles
            /// </summary>
            SpSroUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegRoles
            /// </summary>
            SpSroDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegRoles
            /// </summary>
            spSroInsUpd
        }

        public enum SegRolespagina
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegRolespagina
            /// </summary>
            SpSrpIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegRolespagina
            /// </summary>
            SpSrpUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegRolespagina
            /// </summary>
            SpSrpDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegRolespagina
            /// </summary>
            spSrpInsUpd
        }

        public enum SegRolestablastransacciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegRolestablastransacciones
            /// </summary>
            SpSrtIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegRolestablastransacciones
            /// </summary>
            SpSrtUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegRolestablastransacciones
            /// </summary>
            SpSrtDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegRolestablastransacciones
            /// </summary>
            spSrtInsUpd
        }

        public enum SegTablas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegTablas
            /// </summary>
            SpStaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegTablas
            /// </summary>
            SpStaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegTablas
            /// </summary>
            SpStaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegTablas
            /// </summary>
            spStaInsUpd
        }

        public enum SegTransacciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegTransacciones
            /// </summary>
            SpStrIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegTransacciones
            /// </summary>
            SpStrUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegTransacciones
            /// </summary>
            SpStrDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegTransacciones
            /// </summary>
            spStrInsUpd
        }

        public enum SegTransiciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegTransiciones
            /// </summary>
            SpStsIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegTransiciones
            /// </summary>
            SpStsUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegTransiciones
            /// </summary>
            SpStsDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegTransiciones
            /// </summary>
            spStsInsUpd
        }

        public enum SegUsuarios
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegUsuarios
            /// </summary>
            SpSusIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegUsuarios
            /// </summary>
            SpSusUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegUsuarios
            /// </summary>
            SpSusDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegUsuarios
            /// </summary>
            spSusInsUpd
        }

        public enum SegUsuariosrestriccion
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            SpSurIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            SpSurUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            SpSurDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            spSurInsUpd
        }

        public enum ClaCalificadora
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaCalificadora
            /// </summary>
            SpCcaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaCalificadora
            /// </summary>
            SpCcaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaCalificadora
            /// </summary>
            SpCcaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaCalificadora
            /// </summary>
            spCcaInsUpd
        }

        public enum ClaTipoprospecto
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaTipoprospecto
            /// </summary>
            SpCtpIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaTipoprospecto
            /// </summary>
            SpCtpUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaTipoprospecto
            /// </summary>
            SpCtpDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaTipoprospecto
            /// </summary>
            spCtpInsUpd
        }

        public enum FinAsambleas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinAsambleas
            /// </summary>
            SpFasIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinAsambleas
            /// </summary>
            SpFasUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinAsambleas
            /// </summary>
            SpFasDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinAsambleas
            /// </summary>
            spFasInsUpd
        }

        public enum FinCalificacion
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinCalificacion
            /// </summary>
            SpFcaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinCalificacion
            /// </summary>
            SpFcaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinCalificacion
            /// </summary>
            SpFcaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinCalificacion
            /// </summary>
            spFcaInsUpd
        }

        public enum FinCompromisos
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinCompromisos
            /// </summary>
            SpFcoIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinCompromisos
            /// </summary>
            SpFcoUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinCompromisos
            /// </summary>
            SpFcoDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinCompromisos
            /// </summary>
            spFcoInsUpd
        }

        public enum FinEmisores
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinEmisores
            /// </summary>
            SpFemIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinEmisores
            /// </summary>
            SpFemUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinEmisores
            /// </summary>
            SpFemDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinEmisores
            /// </summary>
            spFemInsUpd
        }

        public enum FinHechos
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinHechos
            /// </summary>
            SpFheIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinHechos
            /// </summary>
            SpFheUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinHechos
            /// </summary>
            SpFheDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinHechos
            /// </summary>
            spFheInsUpd
        }

        public enum FinInformacion
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinInformacion
            /// </summary>
            SpFinIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinInformacion
            /// </summary>
            SpFinUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinInformacion
            /// </summary>
            SpFinDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinInformacion
            /// </summary>
            spFinInsUpd
        }

        public enum FinJuntas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinJuntas
            /// </summary>
            SpFjuIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinJuntas
            /// </summary>
            SpFjuUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinJuntas
            /// </summary>
            SpFjuDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinJuntas
            /// </summary>
            spFjuInsUpd
        }

        public enum FinProspectos
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinProspectos
            /// </summary>
            SpFprIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinProspectos
            /// </summary>
            SpFprUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinProspectos
            /// </summary>
            SpFprDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinProspectos
            /// </summary>
            spFprInsUpd
        }

        public enum FinCupones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinCupones
            /// </summary>
            SpFcvIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinCupones
            /// </summary>
            SpFcvUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinCupones
            /// </summary>
            SpFcvDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinCupones
            /// </summary>
            spFcvInsUpd
        }

        public enum FinSeries
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla FinSeries
            /// </summary>
            SpFseIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla FinSeries
            /// </summary>
            SpFseUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla FinSeries
            /// </summary>
            SpFseDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla FinSeries
            /// </summary>
            spFseInsUpd
        }
    }
}

