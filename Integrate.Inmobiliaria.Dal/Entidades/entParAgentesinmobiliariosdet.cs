#region 
/***********************************************************************************************************
	NOMBRE:       entParAgentesinmobiliariosdet
	DESCRIPCION:
		Clase que define un objeto para la Tabla par_agentes_inmobiliarios_det

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/05/2016  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace Integrate.Inmobiliaria.Dal.Entidades
{
	public class entParAgentesinmobiliariosdet : cBaseClass
	{
		public const String strNombreTabla = "par_agentes_inmobiliarios_det";
		public const String strAliasTabla = "par_agentes_inmobiliarios_det";
		public enum Fields
		{
			agente_inmobiliario
			,telefono
			,tipo_telefono
			,activo

		}
		
		#region Constructoress
		
		public entParAgentesinmobiliariosdet()
		{
			//Inicializacion de Variables
			this.tipo_telefono = null;
		}
		
		public entParAgentesinmobiliariosdet(entParAgentesinmobiliariosdet obj)
		{
			this.agente_inmobiliario = obj.agente_inmobiliario;
			this.telefono = obj.telefono;
			this.tipo_telefono = obj.tipo_telefono;
			this.activo = obj.activo;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna agente_inmobiliario de la Tabla par_agentes_inmobiliarios_det
		/// </summary>
		private int _agente_inmobiliario;
		/// <summary>
		/// 	 Columna de tipo integer de la tabla par_agentes_inmobiliarios_det
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "agente_inmobiliario", Description = "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det")]
		[Required(ErrorMessage = "agente_inmobiliario es un campo requerido.")]
		public int agente_inmobiliario
		{
			get {return _agente_inmobiliario;}
			set
			{
				if (_agente_inmobiliario != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliariosdet.Fields.agente_inmobiliario.ToString());
					_agente_inmobiliario = value;
					RaisePropertyChanged(entParAgentesinmobiliariosdet.Fields.agente_inmobiliario.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna telefono de la Tabla par_agentes_inmobiliarios_det
		/// </summary>
		private int _telefono;
		/// <summary>
		/// 	 Columna de tipo numeric(10, 0) de la tabla par_agentes_inmobiliarios_det
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "telefono", Description = "Columna de tipo numeric(10, 0) de la tabla par_agentes_inmobiliarios_det")]
		[Required(ErrorMessage = "telefono es un campo requerido.")]
		public int telefono
		{
			get {return _telefono;}
			set
			{
				if (_telefono != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliariosdet.Fields.telefono.ToString());
					_telefono = value;
					RaisePropertyChanged(entParAgentesinmobiliariosdet.Fields.telefono.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna tipo_telefono de la Tabla par_agentes_inmobiliarios_det
		/// </summary>
		private String _tipo_telefono;
		/// <summary>
		/// 	 Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios_det
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "tipo_telefono", Description = "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios_det")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "tipo_telefono es un campo requerido.")]
		public String tipo_telefono
		{
			get {return _tipo_telefono;}
			set
			{
				if (_tipo_telefono != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliariosdet.Fields.tipo_telefono.ToString());
					_tipo_telefono = value;
					RaisePropertyChanged(entParAgentesinmobiliariosdet.Fields.tipo_telefono.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna activo de la Tabla par_agentes_inmobiliarios_det
		/// </summary>
		private int _activo;
		/// <summary>
		/// 	 Columna de tipo integer de la tabla par_agentes_inmobiliarios_det
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "activo", Description = "Columna de tipo integer de la tabla par_agentes_inmobiliarios_det")]
		[Required(ErrorMessage = "activo es un campo requerido.")]
		public int activo
		{
			get {return _activo;}
			set
			{
				if (_activo != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliariosdet.Fields.activo.ToString());
					_activo = value;
					RaisePropertyChanged(entParAgentesinmobiliariosdet.Fields.activo.ToString());
				}
			}
		}


	}
}

