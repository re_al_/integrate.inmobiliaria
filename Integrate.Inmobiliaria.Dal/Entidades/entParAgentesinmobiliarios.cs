#region 
/***********************************************************************************************************
	NOMBRE:       entParAgentesinmobiliarios
	DESCRIPCION:
		Clase que define un objeto para la Tabla par_agentes_inmobiliarios

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        18/05/2016  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace Integrate.Inmobiliaria.Dal.Entidades
{
	public class entParAgentesinmobiliarios : cBaseClass
	{
		public const String strNombreTabla = "par_agentes_inmobiliarios";
		public const String strAliasTabla = "par_agentes_inmobiliarios";
		public enum Fields
		{
			agente_inmobiliario
			,descripcion
			,correo_electronico
			,observaciones

		}
		
		#region Constructoress
		
		public entParAgentesinmobiliarios()
		{
			//Inicializacion de Variables
			this.descripcion = null;
			this.correo_electronico = null;
			this.observaciones = null;
		}
		
		public entParAgentesinmobiliarios(entParAgentesinmobiliarios obj)
		{
			this.agente_inmobiliario = obj.agente_inmobiliario;
			this.descripcion = obj.descripcion;
			this.correo_electronico = obj.correo_electronico;
			this.observaciones = obj.observaciones;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna agente_inmobiliario de la Tabla par_agentes_inmobiliarios
		/// </summary>
		private int _agente_inmobiliario;
		/// <summary>
		/// 	 Columna de tipo integer de la tabla par_agentes_inmobiliarios
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "agente_inmobiliario", Description = "Columna de tipo integer de la tabla par_agentes_inmobiliarios")]
		[Required(ErrorMessage = "agente_inmobiliario es un campo requerido.")]
		[Key]
		public int agente_inmobiliario
		{
			get {return _agente_inmobiliario;}
			set
			{
				if (_agente_inmobiliario != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliarios.Fields.agente_inmobiliario.ToString());
					_agente_inmobiliario = value;
					RaisePropertyChanged(entParAgentesinmobiliarios.Fields.agente_inmobiliario.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla par_agentes_inmobiliarios
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "descripcion", Description = "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "descripcion es un campo requerido.")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliarios.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entParAgentesinmobiliarios.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna correo_electronico de la Tabla par_agentes_inmobiliarios
		/// </summary>
		private String _correo_electronico;
		/// <summary>
		/// 	 Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "correo_electronico", Description = "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios")]
		public String correo_electronico
		{
			get {return _correo_electronico;}
			set
			{
				if (_correo_electronico != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliarios.Fields.correo_electronico.ToString());
					_correo_electronico = value;
					RaisePropertyChanged(entParAgentesinmobiliarios.Fields.correo_electronico.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna observaciones de la Tabla par_agentes_inmobiliarios
		/// </summary>
		private String _observaciones;
		/// <summary>
		/// 	 Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "observaciones", Description = "Columna de tipo nvarchar(50) de la tabla par_agentes_inmobiliarios")]
		public String observaciones
		{
			get {return _observaciones;}
			set
			{
				if (_observaciones != value)
				{
					RaisePropertyChanging(entParAgentesinmobiliarios.Fields.observaciones.ToString());
					_observaciones = value;
					RaisePropertyChanged(entParAgentesinmobiliarios.Fields.observaciones.ToString());
				}
			}
		}


	}
}

