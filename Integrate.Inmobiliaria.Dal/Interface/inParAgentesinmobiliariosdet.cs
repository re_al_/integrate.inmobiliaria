#region 
/***********************************************************************************************************
	NOMBRE:       inParAgentesinmobiliariosdet
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla par_agentes_inmobiliarios_det

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/05/2016  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Integrate.Inmobiliaria.Dal; 
using Integrate.Inmobiliaria.SqLiteConn; 
using Integrate.Inmobiliaria.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace Integrate.Inmobiliaria.Dal.Interface
{
	public interface inParAgentesinmobiliariosdet: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entParAgentesinmobiliariosdet.Fields myField);
		entParAgentesinmobiliariosdet ObtenerObjeto(int intagente_inmobiliario, int inttelefono);
		entParAgentesinmobiliariosdet ObtenerObjeto(int intagente_inmobiliario, int inttelefono, ref cTrans localTrans);
		entParAgentesinmobiliariosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entParAgentesinmobiliariosdet ObtenerObjeto(Hashtable htbFiltro);
		entParAgentesinmobiliariosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entParAgentesinmobiliariosdet ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entParAgentesinmobiliariosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entParAgentesinmobiliariosdet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entParAgentesinmobiliariosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entParAgentesinmobiliariosdet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entParAgentesinmobiliariosdet ObtenerObjeto(entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		entParAgentesinmobiliariosdet ObtenerObjeto(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, ref cTrans localTrans);
		entParAgentesinmobiliariosdet ObtenerObjeto(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		entParAgentesinmobiliariosdet ObtenerObjeto(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario();
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entParAgentesinmobiliariosdet> ObtenerDiccionario(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entParAgentesinmobiliariosdet> ObtenerLista();
		List<entParAgentesinmobiliariosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entParAgentesinmobiliariosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entParAgentesinmobiliariosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerLista(entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		List<entParAgentesinmobiliariosdet> ObtenerLista(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerLista(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		List<entParAgentesinmobiliariosdet> ObtenerLista(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entParAgentesinmobiliariosdet> ObtenerLista(Hashtable htbFiltro);
		List<entParAgentesinmobiliariosdet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entParAgentesinmobiliariosdet> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		List<entParAgentesinmobiliariosdet> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entParAgentesinmobiliariosdet> ObtenerCola();
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entParAgentesinmobiliariosdet> ObtenerCola(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entParAgentesinmobiliariosdet> ObtenerPila();
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entParAgentesinmobiliariosdet> ObtenerPila(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entParAgentesinmobiliariosdet obj);
		bool Insert(entParAgentesinmobiliariosdet obj, ref cTrans localTrans);
		int Update(entParAgentesinmobiliariosdet obj);
		int UpdateAll(entParAgentesinmobiliariosdet obj);
		int Update(entParAgentesinmobiliariosdet obj, ref cTrans localTrans);
		int UpdateAll(entParAgentesinmobiliariosdet obj, ref cTrans localTrans);
		int Delete(entParAgentesinmobiliariosdet obj);
		int Delete(entParAgentesinmobiliariosdet obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, entParAgentesinmobiliariosdet.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, entParAgentesinmobiliariosdet.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, String textField, entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, String textField, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, entParAgentesinmobiliariosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, entParAgentesinmobiliariosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, entParAgentesinmobiliariosdet.Fields textField, entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliariosdet.Fields valueField, entParAgentesinmobiliariosdet.Fields textField, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entParAgentesinmobiliariosdet.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entParAgentesinmobiliariosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entParAgentesinmobiliariosdet.Fields refField);
		int FuncionesCount(entParAgentesinmobiliariosdet.Fields refField, entParAgentesinmobiliariosdet.Fields whereField, object valueField);
		int FuncionesCount(entParAgentesinmobiliariosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entParAgentesinmobiliariosdet.Fields refField);
		int FuncionesMin(entParAgentesinmobiliariosdet.Fields refField, entParAgentesinmobiliariosdet.Fields whereField, object valueField);
		int FuncionesMin(entParAgentesinmobiliariosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entParAgentesinmobiliariosdet.Fields refField);
		int FuncionesMax(entParAgentesinmobiliariosdet.Fields refField, entParAgentesinmobiliariosdet.Fields whereField, object valueField);
		int FuncionesMax(entParAgentesinmobiliariosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entParAgentesinmobiliariosdet.Fields refField);
		int FuncionesSum(entParAgentesinmobiliariosdet.Fields refField, entParAgentesinmobiliariosdet.Fields whereField, object valueField);
		int FuncionesSum(entParAgentesinmobiliariosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entParAgentesinmobiliariosdet.Fields refField);
		int FuncionesAvg(entParAgentesinmobiliariosdet.Fields refField, entParAgentesinmobiliariosdet.Fields whereField, object valueField);
		int FuncionesAvg(entParAgentesinmobiliariosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

