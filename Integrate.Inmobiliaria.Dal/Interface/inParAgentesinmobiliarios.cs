#region 
/***********************************************************************************************************
	NOMBRE:       inParAgentesinmobiliarios
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla par_agentes_inmobiliarios

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        18/05/2016  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Integrate.Inmobiliaria.Dal; 
using Integrate.Inmobiliaria.SqLiteConn; 
using Integrate.Inmobiliaria.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace Integrate.Inmobiliaria.Dal.Interface
{
	public interface inParAgentesinmobiliarios: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entParAgentesinmobiliarios.Fields myField);
		entParAgentesinmobiliarios ObtenerObjeto(int intagente_inmobiliario);
		entParAgentesinmobiliarios ObtenerObjeto(int intagente_inmobiliario, ref cTrans localTrans);
		entParAgentesinmobiliarios ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entParAgentesinmobiliarios ObtenerObjeto(Hashtable htbFiltro);
		entParAgentesinmobiliarios ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entParAgentesinmobiliarios ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entParAgentesinmobiliarios ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entParAgentesinmobiliarios ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entParAgentesinmobiliarios ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entParAgentesinmobiliarios ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entParAgentesinmobiliarios ObtenerObjeto(entParAgentesinmobiliarios.Fields searchField, object searchValue);
		entParAgentesinmobiliarios ObtenerObjeto(entParAgentesinmobiliarios.Fields searchField, object searchValue, ref cTrans localTrans);
		entParAgentesinmobiliarios ObtenerObjeto(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		entParAgentesinmobiliarios ObtenerObjeto(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario();
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(entParAgentesinmobiliarios.Fields searchField, object searchValue);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(entParAgentesinmobiliarios.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entParAgentesinmobiliarios> ObtenerDiccionario(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entParAgentesinmobiliarios> ObtenerLista();
		List<entParAgentesinmobiliarios> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entParAgentesinmobiliarios> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entParAgentesinmobiliarios> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerLista(entParAgentesinmobiliarios.Fields searchField, object searchValue);
		List<entParAgentesinmobiliarios> ObtenerLista(entParAgentesinmobiliarios.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerLista(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		List<entParAgentesinmobiliarios> ObtenerLista(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entParAgentesinmobiliarios> ObtenerLista(Hashtable htbFiltro);
		List<entParAgentesinmobiliarios> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entParAgentesinmobiliarios> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliarios.Fields searchField, object searchValue);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliarios.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		List<entParAgentesinmobiliarios> ObtenerListaDesdeVista(String strVista, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entParAgentesinmobiliarios> ObtenerCola();
		Queue<entParAgentesinmobiliarios> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entParAgentesinmobiliarios> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entParAgentesinmobiliarios> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entParAgentesinmobiliarios> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entParAgentesinmobiliarios> ObtenerCola(entParAgentesinmobiliarios.Fields searchField, object searchValue);
		Queue<entParAgentesinmobiliarios> ObtenerCola(entParAgentesinmobiliarios.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entParAgentesinmobiliarios> ObtenerCola(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entParAgentesinmobiliarios> ObtenerCola(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entParAgentesinmobiliarios> ObtenerPila();
		Stack<entParAgentesinmobiliarios> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entParAgentesinmobiliarios> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entParAgentesinmobiliarios> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entParAgentesinmobiliarios> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entParAgentesinmobiliarios> ObtenerPila(entParAgentesinmobiliarios.Fields searchField, object searchValue);
		Stack<entParAgentesinmobiliarios> ObtenerPila(entParAgentesinmobiliarios.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entParAgentesinmobiliarios> ObtenerPila(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entParAgentesinmobiliarios> ObtenerPila(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entParAgentesinmobiliarios obj);
		bool Insert(entParAgentesinmobiliarios obj, ref cTrans localTrans);
		int Update(entParAgentesinmobiliarios obj);
		int UpdateAll(entParAgentesinmobiliarios obj);
		int Update(entParAgentesinmobiliarios obj, ref cTrans localTrans);
		int UpdateAll(entParAgentesinmobiliarios obj, ref cTrans localTrans);
		int Delete(entParAgentesinmobiliarios obj);
		int Delete(entParAgentesinmobiliarios obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entParAgentesinmobiliarios.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entParAgentesinmobiliarios.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, entParAgentesinmobiliarios.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, entParAgentesinmobiliarios.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, String textField, entParAgentesinmobiliarios.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, String textField, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, entParAgentesinmobiliarios.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, entParAgentesinmobiliarios.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, entParAgentesinmobiliarios.Fields textField, entParAgentesinmobiliarios.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entParAgentesinmobiliarios.Fields valueField, entParAgentesinmobiliarios.Fields textField, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entParAgentesinmobiliarios.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entParAgentesinmobiliarios.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entParAgentesinmobiliarios.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entParAgentesinmobiliarios.Fields refField);
		int FuncionesCount(entParAgentesinmobiliarios.Fields refField, entParAgentesinmobiliarios.Fields whereField, object valueField);
		int FuncionesCount(entParAgentesinmobiliarios.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entParAgentesinmobiliarios.Fields refField);
		int FuncionesMin(entParAgentesinmobiliarios.Fields refField, entParAgentesinmobiliarios.Fields whereField, object valueField);
		int FuncionesMin(entParAgentesinmobiliarios.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entParAgentesinmobiliarios.Fields refField);
		int FuncionesMax(entParAgentesinmobiliarios.Fields refField, entParAgentesinmobiliarios.Fields whereField, object valueField);
		int FuncionesMax(entParAgentesinmobiliarios.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entParAgentesinmobiliarios.Fields refField);
		int FuncionesSum(entParAgentesinmobiliarios.Fields refField, entParAgentesinmobiliarios.Fields whereField, object valueField);
		int FuncionesSum(entParAgentesinmobiliarios.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entParAgentesinmobiliarios.Fields refField);
		int FuncionesAvg(entParAgentesinmobiliarios.Fields refField, entParAgentesinmobiliarios.Fields whereField, object valueField);
		int FuncionesAvg(entParAgentesinmobiliarios.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

