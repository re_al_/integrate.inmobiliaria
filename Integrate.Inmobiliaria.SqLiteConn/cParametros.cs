﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integrate.Inmobiliaria.SqLiteConn
{
    static public class cParametros
    {
        public static string schema = "";
        //PRODUCCION SIAPS
        public static string bdFile = @"bd/inmobiliaria.db";

        //Otros parametros
        public static string parFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
        public static string parFormatoFecha = "dd/MM/yyyy";
    }
}
