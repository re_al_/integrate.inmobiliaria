﻿namespace Integrate.Inmobiliaria.App
{
    partial class fBuscador
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcPacPaciente = new DevExpress.XtraGrid.GridControl();
            this.gvPacPaciente = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcPacPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPacPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPacPaciente
            // 
            this.gcPacPaciente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPacPaciente.Location = new System.Drawing.Point(0, 0);
            this.gcPacPaciente.MainView = this.gvPacPaciente;
            this.gcPacPaciente.Name = "gcPacPaciente";
            this.gcPacPaciente.Size = new System.Drawing.Size(936, 396);
            this.gcPacPaciente.TabIndex = 0;
            this.gcPacPaciente.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPacPaciente});
            // 
            // gvPacPaciente
            // 
            this.gvPacPaciente.GridControl = this.gcPacPaciente;
            this.gvPacPaciente.Name = "gvPacPaciente";
            this.gvPacPaciente.OptionsBehavior.Editable = false;
            // 
            // fBuscador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 396);
            this.Controls.Add(this.gcPacPaciente);
            this.Name = "fBuscador";
            this.Text = "Agentes Inmobiliarios";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcPacPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPacPaciente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcPacPaciente;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPacPaciente;
    }
}

