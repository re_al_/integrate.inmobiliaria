﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Integrate.Inmobiliaria.Dal.Entidades;
using Integrate.Inmobiliaria.Dal.Modelo;

namespace Integrate.Inmobiliaria.App
{
    public partial class fAgentes : Form
    {
        private entParAgentesinmobiliarios _myObj = null;
        public fAgentes()
        {
            InitializeComponent();
        }
        public fAgentes(entParAgentesinmobiliarios obj)
        {
            InitializeComponent();
            _myObj = obj;
        }

        private void frmAgendaDet_Load(object sender, EventArgs e)
        {
            if (_myObj != null)
            {
                txtNombre.Text = _myObj.descripcion;
                txtCorreo.Text = _myObj.correo_electronico;
                txtObservaciones.Text = _myObj.observaciones;

                CargarInmobiliarioDet();
            }
            else
            {
                btnEliminarDetalle.Enabled = false;
                btnNuevoDetalle.Enabled = false;
            }
        }

        private void CargarInmobiliarioDet()
        {
            try
            {
                rnParAgentesinmobiliariosdet rn = new rnParAgentesinmobiliariosdet();
                DataTable dt = rn.ObtenerDataTable(entParAgentesinmobiliariosdet.Fields.agente_inmobiliario, _myObj.agente_inmobiliario);
                dtgDetalle.DataSource = dt;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;
            try
            {
                rnParAgentesinmobiliarios rn = new rnParAgentesinmobiliarios();

                if (_myObj == null)
                {
                    entParAgentesinmobiliarios obj = new entParAgentesinmobiliarios();

                    obj.descripcion = txtNombre.Text;
                    obj.correo_electronico = txtCorreo.Text;
                    obj.observaciones = txtObservaciones.Text;

                    rn.Insert(obj);

                    int idMax = rn.FuncionesMax(entParAgentesinmobiliarios.Fields.agente_inmobiliario);

                    _myObj = rn.ObtenerObjeto(idMax);


                    btnEliminarDetalle.Enabled = true;
                    btnNuevoDetalle.Enabled = true;
                }
                else
                {
                    _myObj.descripcion = txtNombre.Text;
                    _myObj.correo_electronico = txtCorreo.Text;
                    _myObj.observaciones = txtObservaciones.Text;

                    rn.Update(_myObj);

                    this.DialogResult = DialogResult.OK;
                    Close();
                }                
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void btnEliminarDetalle_Click(object sender, EventArgs e)
        {
            if (dtgDetalle.Rows.Count == 0)
                return;

            try
            {
                string idAgente =
                    dtgDetalle[
                        entParAgentesinmobiliarios.Fields.agente_inmobiliario.ToString(),
                        dtgDetalle.SelectedRows[0].Index].Value as string;

                rnParAgentesinmobiliarios rn = new rnParAgentesinmobiliarios();
                entParAgentesinmobiliarios obj = rn.ObtenerObjeto(int.Parse(idAgente));

                if (
                    MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar registro",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    rn.Delete(obj);
                    CargarInmobiliarioDet();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void btnNuevoDetalle_Click(object sender, EventArgs e)
        {
            fAgentesTel frm = new fAgentesTel(_myObj);
            if (frm.ShowDialog() == DialogResult.OK)
                CargarInmobiliarioDet();
        }

        private void dtgDetalle_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dtgDetalle.Rows.Count == 0)
                return;

            try
            {
                string idAgente =
                    dtgDetalle[
                        entParAgentesinmobiliariosdet.Fields.agente_inmobiliario.ToString(),
                        dtgDetalle.SelectedRows[0].Index].Value.ToString();

                string idTelefono =dtgDetalle[
                        entParAgentesinmobiliariosdet.Fields.telefono.ToString(),
                        dtgDetalle.SelectedRows[0].Index].Value.ToString();

                rnParAgentesinmobiliariosdet rn = new rnParAgentesinmobiliariosdet();
                entParAgentesinmobiliariosdet obj = rn.ObtenerObjeto(int.Parse(idAgente), int.Parse(idTelefono));

                fAgentesTel frm = new fAgentesTel(_myObj, obj);
                if (frm.ShowDialog() == DialogResult.OK)
                    CargarInmobiliarioDet();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }
    }
}
