﻿namespace Integrate.Inmobiliaria.App
{
    partial class fPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fPrincipal));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.bsiCopyrights = new DevExpress.XtraBars.BarStaticItem();
            this.bbiBuscador = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAgentes = new DevExpress.XtraBars.BarButtonItem();
            this.bmcVentanas = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.rpSistema = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgMenu = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgVentanas = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ApplicationIcon = global::Integrate.Inmobiliaria.App.Properties.Resources.INTEGRATE;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bsiCopyrights,
            this.bbiBuscador,
            this.bbiAgentes,
            this.bmcVentanas});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 1;
            this.ribbon.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpSistema});
            this.ribbon.Size = new System.Drawing.Size(1016, 144);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            // 
            // bsiCopyrights
            // 
            this.bsiCopyrights.Id = 48;
            this.bsiCopyrights.Name = "bsiCopyrights";
            this.bsiCopyrights.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiCopyrights.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiCopyrights_ItemClick);
            this.bsiCopyrights.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiCopyrights_ItemDoubleClick);
            // 
            // bbiBuscador
            // 
            this.bbiBuscador.Caption = "Buscador de Teléfonos";
            this.bbiBuscador.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiBuscador.Id = 49;
            this.bbiBuscador.LargeGlyph = global::Integrate.Inmobiliaria.App.Properties.Resources.telefonos;
            this.bbiBuscador.LargeWidth = 85;
            this.bbiBuscador.Name = "bbiBuscador";
            this.bbiBuscador.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiBuscador.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBuscador_ItemClick);
            // 
            // bbiAgentes
            // 
            this.bbiAgentes.Caption = "Agentes Inmobiliarios";
            this.bbiAgentes.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiAgentes.Id = 50;
            this.bbiAgentes.LargeGlyph = global::Integrate.Inmobiliaria.App.Properties.Resources.agentes;
            this.bbiAgentes.LargeWidth = 85;
            this.bbiAgentes.Name = "bbiAgentes";
            this.bbiAgentes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAgentes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAgentes_ItemClick);
            // 
            // bmcVentanas
            // 
            this.bmcVentanas.Caption = "Ventanas activas";
            this.bmcVentanas.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bmcVentanas.Id = 51;
            this.bmcVentanas.LargeGlyph = global::Integrate.Inmobiliaria.App.Properties.Resources.windowlist;
            this.bmcVentanas.LargeWidth = 85;
            this.bmcVentanas.Name = "bmcVentanas";
            this.bmcVentanas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // rpSistema
            // 
            this.rpSistema.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgMenu,
            this.rpgVentanas});
            this.rpSistema.Name = "rpSistema";
            this.rpSistema.Text = "Sistema";
            // 
            // rpgMenu
            // 
            this.rpgMenu.ItemLinks.Add(this.bbiBuscador);
            this.rpgMenu.ItemLinks.Add(this.bbiAgentes);
            this.rpgMenu.Name = "rpgMenu";
            this.rpgMenu.Text = "Menu";
            // 
            // rpgVentanas
            // 
            this.rpgVentanas.ItemLinks.Add(this.bmcVentanas);
            this.rpgVentanas.Name = "rpgVentanas";
            this.rpgVentanas.Text = "Ventanas";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.bsiCopyrights);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 736);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1016, 31);
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // fPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 767);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "fPrincipal";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "INTEGRATE - Inmobiliaria";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.fPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        public DevExpress.XtraBars.Ribbon.RibbonPage rpSistema;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarStaticItem bsiCopyrights;
        private DevExpress.XtraBars.BarButtonItem bbiBuscador;
        private DevExpress.XtraBars.BarButtonItem bbiAgentes;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgMenu;
        private DevExpress.XtraBars.BarMdiChildrenListItem bmcVentanas;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgVentanas;
    }
}