﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Integrate.Inmobiliaria.Dal.Entidades;
using Integrate.Inmobiliaria.Dal.Modelo;

namespace Integrate.Inmobiliaria.App
{
    public partial class lAgentes : Form
    {
        public lAgentes()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarListado();
        }

        public void CargarListado()
        {
            try
            {
                rnParAgentesinmobiliarios rn = new rnParAgentesinmobiliarios();
                DataTable dt = rn.ObtenerDataTable();
                gcPacPaciente.DataSource = dt;
                if (dt.Columns.Count > 0)
                {
                    gvPacPaciente.PopulateColumns();
                    gvPacPaciente.OptionsBehavior.Editable = false;
                    
                    gvPacPaciente.ShowFindPanel();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea DAR DE BAJA el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    string idAgente = gvPacPaciente.GetFocusedRowCellValue(entParAgentesinmobiliarios.Fields.agente_inmobiliario.ToString()).ToString();
                    rnParAgentesinmobiliarios rn = new rnParAgentesinmobiliarios();
                    entParAgentesinmobiliarios obj = rn.ObtenerObjeto(int.Parse(idAgente));

                    if (obj != null)
                    {
                        //Abrimos la AUDITORIA
                        rn.Delete(obj);
                        CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string idAgente = gvPacPaciente.GetFocusedRowCellValue(entParAgentesinmobiliarios.Fields.agente_inmobiliario.ToString()).ToString();

                rnParAgentesinmobiliarios rn = new rnParAgentesinmobiliarios();
                entParAgentesinmobiliarios obj = rn.ObtenerObjeto(int.Parse(idAgente));

                fAgentes frm = new fAgentes(obj);
                if (frm.ShowDialog() == DialogResult.OK)
                    CargarListado();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            fAgentes frm = new fAgentes();
            if (frm.ShowDialog() == DialogResult.OK)
                CargarListado();
        }

        private void lAgentes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B && e.Modifiers == Keys.Control)
            {
                gvPacPaciente.ShowFindPanel();
            }
        }

        private void gvPacPaciente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B && e.Modifiers == Keys.Control)
            {
                gvPacPaciente.ShowFindPanel();
            }
        }

        private void gcPacPaciente_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                string idAgente = gvPacPaciente.GetFocusedRowCellValue(entParAgentesinmobiliarios.Fields.agente_inmobiliario.ToString()).ToString();

                rnParAgentesinmobiliarios rn = new rnParAgentesinmobiliarios();
                entParAgentesinmobiliarios obj = rn.ObtenerObjeto(int.Parse(idAgente));

                fAgentes frm = new fAgentes(obj);
                if (frm.ShowDialog() == DialogResult.OK)
                    CargarListado();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }
    }
}
