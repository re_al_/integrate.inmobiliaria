﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace Integrate.Inmobiliaria.App
{

    public partial class fPrincipal : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public fPrincipal()
        {
            InitializeComponent();
        }

        private void fPrincipal_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            //Las Leyendas del Pie de Pagina
            //Las Leyendas del Pie de Pagina
            if (DateTime.Now.Year == 2016)
                bsiCopyrights.Caption = "© 2016 INTEGRATE Soluciones Informáticas - Todos los Derechos Reservados";
            else
                bsiCopyrights.Caption = "© 2016 - " + DateTime.Now.Year + " INTEGRATE Soluciones Informáticas - Todos los Derechos Reservados";

            //Habilitamos las acciones segun el Usuario
            HabilitarAcciones();

            //Para Abrir un  Form Hijo
            fBuscador frm = new fBuscador();
            frm.MdiParent = this;
            frm.Show();
        }

        public void HabilitarAcciones()
        {
            try
            {
            }
            catch (Exception exp)
            {
                //fErrores frm = new fErrores(exp, fErrores.tipoError.error, "Error");
                //frm.ShowDialog();
            }
        }

        private void bbiAgentes_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                bool bProcede = true;
                foreach (Form mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(lAgentes))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                        ((lAgentes)mdiChild).CargarListado();
                    }
                }
                if (bProcede)
                {
                    lAgentes frm = new lAgentes();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void bbiBuscador_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                bool bProcede = true;
                foreach (Form mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(fBuscador))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                        ((fBuscador)mdiChild).CargarListado();
                    }
                }
                if (bProcede)
                {
                    fBuscador frm = new fBuscador();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }

        private void bsiCopyrights_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://integrate.com.bo/");
            Process.Start(sInfo);
        }

        private void bsiCopyrights_ItemClick(object sender, ItemClickEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://integrate.com.bo/");
            Process.Start(sInfo);
        }
    }
}