﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Integrate.Inmobiliaria.Dal.Entidades;
using Integrate.Inmobiliaria.Dal.Modelo;

namespace Integrate.Inmobiliaria.App
{
    public partial class fAgentesTel : Form
    {
        private entParAgentesinmobiliariosdet _myObj = null;
        private entParAgentesinmobiliarios _myObjPadre = null;

        public fAgentesTel(entParAgentesinmobiliarios objPadre)
        {
            InitializeComponent();

            _myObjPadre = objPadre;
        }

        public fAgentesTel(entParAgentesinmobiliarios objPadre, entParAgentesinmobiliariosdet obj)
        {
            InitializeComponent();

            _myObjPadre = objPadre;
            _myObj = obj;
        }

        private void frmAgendaDetTel_Load(object sender, EventArgs e)
        {
            cmbTipo.SelectedIndex = 0;

            if (_myObj != null)
            {
                cmbTipo.SelectedIndex = _myObj.tipo_telefono == "F" ? 0 : 1;
                txtTelefono.Text = _myObj.telefono.ToString();
                chkActivo.Checked = _myObj.activo == 1;
            }
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;
            try
            {
                rnParAgentesinmobiliariosdet rn = new rnParAgentesinmobiliariosdet();

                if (_myObj == null)
                {
                    entParAgentesinmobiliariosdet obj = new entParAgentesinmobiliariosdet();

                    obj.agente_inmobiliario = _myObjPadre.agente_inmobiliario;
                    obj.telefono = txtTelefono.Text.Trim() == "" ? 0 : int.Parse(txtTelefono.Text);
                    obj.tipo_telefono = cmbTipo.SelectedIndex == 0 ? "F" : "M";
                    obj.activo = chkActivo.Checked ? 1 : 0;

                    rn.Insert(obj);

                    bProcede = true;
                }
                else
                {
                    _myObj.telefono = txtTelefono.Text.Trim() == "" ? 0 : int.Parse(txtTelefono.Text);
                    _myObj.tipo_telefono = cmbTipo.SelectedIndex == 0 ? "F" : "M";
                    _myObj.activo = chkActivo.Checked ? 1 : 0;

                    rn.Update(_myObj);
                }

                if (bProcede)
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }
    }
}
