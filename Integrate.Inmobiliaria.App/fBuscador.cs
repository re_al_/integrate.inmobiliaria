﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Integrate.Inmobiliaria.Dal.Entidades;
using Integrate.Inmobiliaria.Dal.Modelo;

namespace Integrate.Inmobiliaria.App
{
    public partial class fBuscador : Form
    {
        public fBuscador()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarListado();
        }

        public void CargarListado()
        {
            try
            {
                rnVista rn = new rnVista();
                DataTable dt = rn.ObtenerDatos("vw_buscar");

                gcPacPaciente.DataSource = dt;
                if (dt.Columns.Count > 0)
                {
                    gvPacPaciente.PopulateColumns();
                    gvPacPaciente.OptionsBehavior.Editable = false;
                    gvPacPaciente.ShowFindPanel();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
            }
        }
    }
}
