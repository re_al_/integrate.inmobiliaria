﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Integrate.Inmobiliaria.App
{
    
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Inicializamos al Paciente
            Application.Run(new fPrincipal());
            
        }
    }
}
